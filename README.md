# Signature pad

Provides a widget for image fields to draw
with the [Signature pad](https://github.com/szimek/signature_pad) js library.

## Installation

In the form display of any entity with an image field
select the "Signature pad" widget type.

The JS libraries are fetched from the jsdelivr CDN.

Using SVG for images is not well supported in Drupal so ensure your image
toolkit supports it, for example [Imagemagick](https://www.drupal.org/project/imagemagick).

## Features

* Support different image formats (save as svg, png or jpg).
* Choose initial background color.
* Save image raw data (lines and dots) to edit later.
* Configure height, width or aspect ratio for the canvas.
* Require a minimum of strokes to pass validation.
* Enable the user to change pen color with the [jscolor](https://jscolor.com) picker and pen size.
* Undo button.

## Similar modules

Other modules offer a different set of features
and they are focused on the original purpose of the signature pad
library that is obviously signatures.
This modules to allow the user
to have more options for drawing simple images.

[Signature pad widget](https://www.drupal.org/project/sign_widget)

* Only supports PNG
* Options like the pen color are set when configuring the widget instead of giving the choice to the user.
* Depends on jQuery.
* Images can be displayed with an image formatter and draw over them
 (the same can be achieved with this module but still requires to configure an image widget instead of a formatter).

[Signature field](https://www.drupal.org/project/signature_field)

* Use a custom field type instead of image fields from Drupal core.

[Signature (Field)](https://www.drupal.org/project/signaturefield)

* Same as signature field

[Webform](https://www.drupal.org/project/webform)

* Only for webform elements not Drupal fields.
* Uses an outdated version of signature pad.
