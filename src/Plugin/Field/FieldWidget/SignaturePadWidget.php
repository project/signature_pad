<?php

namespace Drupal\signature_pad\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\signature_pad\RenderCallback;
use PNGMetadata\PNGMetadata;

/**
 * Defines the 'signature_pad' field widget.
 *
 * @FieldWidget(
 *   id = "signature_pad",
 *   label = @Translation("Signature pad"),
 *   field_types = {"image"},
 * )
 */
class SignaturePadWidget extends ImageWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() : array {
    return [
      'background_color' => 'rgba(0,0,0,0)',
      'pen_color' => '#00000',
      'aspect_ratio' => '16:10',
      'format' => 'image/png',
      'filename' => 'signature',
      'min_strokes' => 0,
      'min_strokes_message' => '',
      'save_json_data' => FALSE,
      'fixed_width' => 0,
      'fixed_height' => 0,
      'dimensions_from_image' => FALSE,
      'reset_button' => 'button',
      'color_picker' => '',
      'size_picker' => 'slider',
      'undo' => 'button',
      'hide_image_link' => TRUE,
      'hide_remove_button' => TRUE,
      'hide_alt_field' => TRUE,
      'hide_title_field' => TRUE,
      'preview_image_style' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState) : array {
    $element = parent::settingsForm($form, $formState);

    // There is no upload button that triggers the ajax upload
    // so there is no progress displayed.
    $element['progress_indicator']['#access'] = FALSE;

    $element['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Image format'),
      '#default_value' => $this->getSetting('format'),
      '#options' => self::formatsMap(),
    ];

    $fieldExtensions = $this->getFieldSetting('file_extensions');
    $supportedExtensions = $this->imageFactory->getSupportedExtensions();
    $extensions = !empty($fieldExtensions) ? array_intersect(explode(' ', $fieldExtensions), $supportedExtensions) : $supportedExtensions;
    if (!in_array('svg', $extensions, TRUE)) {
      unset($element['format']['#options']['image/svg+xml']);
      $element['format']['#description'] = $this->t(
        'For SVG support an image toolkit that supports SVG needs to be installed, for example <a href="@imagemagick">ImageMagick</a>',
        ['@imagemagick' => 'https://www.drupal.org/project/imagemagick']
      );
    }

    $element['background_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background color'),
      '#default_value' => $this->getSetting('background_color'),
      '#description' => $this->t('Can be any color format accepted by context.fillStyle. Defaults to "rgba(0,0,0,0)" (transparent black). Use a non-transparent color e.g. "rgb(255,255,255)" (opaque white) to save signatures as JPEG images.'),
    ];
    $element['pen_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Initial pen color'),
      '#default_value' => $this->getSetting('pen_color'),
    ];
    $element['aspect_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Aspect ratio'),
      '#default_value' => $this->getSetting('aspect_ratio'),
      '#description' => $this->t('Proportions for the canvas in the format WIDTH:HEIGHT.'),
    ];
    $element['fixed_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Fixed width'),
      '#description' => $this->t('Set the width in pixels for the canvas. "0" means it will take the width of its container.'),
      '#default_value' => $this->getSetting('fixed_width'),
    ];
    $element['fixed_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Fixed height'),
      '#description' => $this->t('Set the height in pixels for the canvas. "0" means it will respect the aspect ratio set above.'),
      '#default_value' => $this->getSetting('fixed_height'),
    ];
    $element['dimensions_from_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dimensions from image'),
      '#description' => $this->t('Set the width and height of the canvas from the image being loaded. When no image is loaded the above settings determine the dimensions.'),
      '#default_value' => $this->getSetting('dimensions_from_image'),
    ];
    $element['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File name (without extension)'),
      '#description' => $this->t('The extension is given by the image format'),
      '#default_value' => $this->getSetting('filename'),
    ];
    $element['min_strokes'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum number of strokes to pass validation'),
      '#description' => $this->t('Avoid a signature that is too simple. "0" means no requirement'),
      '#default_value' => $this->getSetting('min_strokes'),
    ];
    $element['min_strokes_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum strokes message'),
      '#placeholder' => 'Please add some strokes to the signature',
      '#description' => $this->t('Error message displayed when the minimum number of strokes are not met'),
      '#default_value' => $this->getSetting('min_strokes_message'),
    ];
    $element['save_json_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save json data'),
      '#description' => $this->t('Store the raw data of the strokes in the PNG/SVG file as metadata. The metadata will be used to initialize the signature pad when editing. When using svg as format this setting is highly recommended since the data is needed to load an SVG image and save it back again as SVG. The metadata is preferred when is present, therefore, enabling this and editing an image with no previous metadata will eventually lead to lose the image that was stored.'),
      '#default_value' => $this->getSetting('save_json_data'),
    ];

    $element['reset_button'] = [
      '#type' => 'select',
      '#title' => $this->t('Reset button'),
      '#default_value' => $this->getSetting('reset_button'),
      '#options' => [
        '' => $this->t('No reset button'),
        'button' => $this->t('Button'),
      ],
    ];

    $element['color_picker'] = [
      '#type' => 'select',
      '#title' => $this->t('Color picker'),
      '#default_value' => $this->getSetting('color_picker'),
      '#options' => [
        '' => $this->t('No color picker'),
        'native' => $this->t('Browser native color picker'),
        'jscolor' => $this->t('jscolor (see more at jscolor.com)'),
      ],
    ];

    $element['size_picker'] = [
      '#type' => 'select',
      '#title' => $this->t('Pen size picker'),
      '#default_value' => $this->getSetting('size_picker'),
      '#options' => [
        '' => $this->t('No size picker'),
        'slider' => $this->t('Range slider'),
      ],
    ];

    $element['undo'] = [
      '#type' => 'select',
      '#title' => $this->t('Undo'),
      '#default_value' => $this->getSetting('undo'),
      '#options' => [
        '' => $this->t('Disable'),
        'button' => $this->t('Button'),
      ],
    ];
    $element['hide_image_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide image link'),
      '#default_value' => $this->getSetting('hide_image_link'),
    ];
    $element['hide_remove_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide "Remove" button'),
      '#default_value' => $this->getSetting('hide_remove_button'),
    ];
    $element['hide_alt_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Alt field'),
      '#description' => $this->t('It can still be enabled but not displayed in the context of this widget.'),
      '#default_value' => $this->getSetting('hide_alt_field'),
    ];
    $element['hide_title_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Title field'),
      '#description' => $this->t('It can still be enabled but not displayed in the context of this widget.'),
      '#default_value' => $this->getSetting('hide_title_field'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() : array {
    $summary = [];
    $summary[] = $this->t('Background color: @background_color', ['@background_color' => $this->getSetting('background_color')]);
    $summary[] = $this->t('Format: @format', ['@format' => $this->getSetting('format')]);
    if ($this->getSetting('fixed_width')) {
      $summary[] = $this->t('Width: @fixed_width', ['@fixed_width' => $this->getSetting('fixed_width')]);
    }
    if ($this->getSetting('fixed_height')) {
      $summary[] = $this->t('Height: @fixed_height', ['@fixed_height' => $this->getSetting('fixed_height')]);
    }
    else {
      $summary[] = $this->t('Aspect ratio: @aspect_ratio', ['@aspect_ratio' => $this->getSetting('aspect_ratio')]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $formState) : array {
    $element = parent::formElement($items, $delta, $element, $form, $formState);

    $element['#attached']['library'][] = 'signature_pad/widget.signature_pad';

    $extension = self::formatsMap()[$this->getSetting('format')];
    $minStrokes = intval($this->getSetting('min_strokes'));
    $element['#signature_pad_settings'] = [
      'backgroundColor' => $this->getSetting('background_color'),
      'penColor' => $this->getSetting('pen_color'),
      'aspectRatio' => $this->getSetting('aspect_ratio'),
      'format' => $this->getSetting('format'),
      'extension' => $extension,
      'filename' => $this->getSetting('filename'),
      'saveJsonData' => $this->getSetting('save_json_data'),
      'fixedWidth' => intval($this->getSetting('fixed_width')),
      'fixedHeight' => intval($this->getSetting('fixed_height')),
      'dimensionsFromImage' => $this->getSetting('dimensions_from_image'),
      'hideImageLink' => $this->getSetting('hide_image_link'),
      'hideRemoveButton' => $this->getSetting('hide_remove_button'),
      'hideAltField' => $this->getSetting('hide_alt_field'),
      'hideTitleField' => $this->getSetting('hide_title_field'),
    ];

    // The widget only allows one format/extension.
    $element['#upload_validators']['FileExtension']['extensions'] = $extension;

    // Validate the strokes in the backend using the metadata.
    if (
      $this->getSetting('save_json_data') &&
      $minStrokes &&
      ($extension === 'png' || $extension === 'svg')
    ) {
      $element['#upload_validators']['SignaturePadMinStrokes'] = [
        'minStrokes' => $minStrokes,
        'format' => $this->getSetting('format'),
      ];
      if (!empty($this->getSetting('min_strokes_message'))) {
        $element['#upload_validators']['SignaturePadMinStrokes']['message'] = $this->getSetting('min_strokes_message');
      }
    }

    $element['signature_pad_canvas'] = [
      '#type' => 'html_tag',
      '#tag' => 'canvas',
      '#attributes' => [
        'class' => ['signature-pad-canvas'],
      ],
    ];

    /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $imageItem */
    $imageItem = $items->get($delta);

    // Other formats different than SVG need async processing to be saved before submitting.
    if ($this->getSetting('format') !== 'image/svg+xml') {
      $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-processing-scheduled'] = 'true';
    }

    if ($this->getSetting('save_json_data') && $this->getSetting('format') === 'image/png') {
      $element['#attached']['library'][] = 'signature_pad/store-source-png';
      if (!$imageItem->isEmpty() && $initData = $this->getInitDataPng($imageItem)) {
        $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-data'] = $initData;
      }
    }

    if ($this->getSetting('format') === 'image/svg+xml') {
      $element['#attached']['library'][] = 'signature_pad/store-source-svg';
      if ($this->getSetting('save_json_data') && $initData = $this->getInitDataSvg($imageItem)) {
        $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-data'] = $initData;
      }
    }

    // Having two different kinds of init data will complicate the rendering
    // as one can draw over the other. Therefore only load raster data when
    // there is no signature pad data present from the metadata.
    // Note that even SVGs are treated as raster data by signature pad
    // so loading an SVG without the JSON data will not save the initial
    // SVG back again, only the new drawings made to it.
    if (!isset($element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-data']) && !$imageItem->isEmpty()) {
      $initUri = 'data:' . $imageItem->entity->getMimeType() . ';base64,' . base64_encode(file_get_contents($imageItem->entity->getFileUri()));
      $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-uri'] = $initUri;
      $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-uri-width'] = $imageItem->get('width')->getValue();
      $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-init-uri-height'] = $imageItem->get('height')->getValue();
      if ($this->getSetting('dimensions_from_image')) {
        $element['#signature_pad_settings']['fixedWidth'] = $imageItem->get('width')->getValue();
        $element['#signature_pad_settings']['fixedHeight'] = $imageItem->get('height')->getValue();
      }
    }

    if ($this->getSetting('reset_button') === 'button') {
      $element['signature_pad_reset'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Clear'),
        '#attributes' => [
          'class' => [
            'button',
            'button--extrasmall',
            'signature-pad-reset-button',
          ],
        ],
      ];
    }
    if ($this->getSetting('undo') === 'button') {
      $element['signature_pad_undo'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Undo'),
        '#attributes' => [
          'class' => [
            'button',
            'button--extrasmall',
            'signature-pad-undo-button',
          ],
        ],
      ];
    }

    if ($this->getSetting('color_picker') === 'native') {
      $element['signature_pad_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Pen color'),
        '#default_value' => $this->getSetting('pen_color'),
        '#attributes' => [
          'class' => [
            'button',
            'button--extrasmall',
            'signature-pad-color-picker-native',
          ],
        ],
      ];
    }

    if ($this->getSetting('color_picker') === 'jscolor') {
      $element['#attached']['library'][] = 'signature_pad/jscolor';
      $element['signature_pad_color'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#value' => $this->t('Pen color'),
        '#attributes' => [
          'class' => [
            'button',
            'button--extrasmall',
            'signature-pad-color-picker-jscolor',
          ],
          'data-jscolor' => json_encode([
            'value' => $this->getSetting('pen_color'),
            'onChange' => 'this.previewElement.parentElement.querySelector("canvas").signaturePad.penColor = this.toRGBAString();',
            'format' => 'rgba',
            'palette' => [
              '#000000', '#7d7d7d', '#870014', '#ec1c23', '#ff7e26',
              '#fef100', '#22b14b', '#00a1e7', '#3f47cc', '#a349a4',
              '#ffffff', '#c3c3c3', '#b87957', '#feaec9', '#ffc80d',
              '#eee3af', '#b5e61d', '#99d9ea', '#7092be', '#c8bfe7',
            ],
            // Autohide does not work when clicking on the canvas.
            'hideOnPaletteClick' => TRUE,
          ]),
        ],
      ];
    }

    if ($this->getSetting('size_picker') === 'slider') {
      $element['signature_pad_size_picker'] = [
        '#type' => 'range',
        '#title' => $this->t('Pen size'),
        '#change_labels_title_display' => 'before',
        '#default_value' => 2,
        '#min' => 1,
        '#max' => 25,
        '#attributes' => [
          'class' => [
            'button',
            'button--extrasmall',
            'signature-pad-size-picker-slider',
          ],
        ],
      ];
    }

    if ($minStrokes) {
      $element['signature_pad_strokes'] = [
        '#type' => 'number',
        // @todo Use number of current strokes from the image metadata.
        '#default_value' => $element['#required'] ? 0 : $minStrokes,
        '#min' => $minStrokes,
        '#required' => TRUE,
        '#wrapper_attributes' => [
          'class' => [
            'visually-hidden',
          ],
        ],
        '#attributes' => [
          'class' => [
            'signature-pad-strokes',
          ],
          'onchange' => 'if (!this.validity.rangeUnderflow) {this.setCustomValidity("");}',
          'oninvalid' => 'if (this.validity.rangeUnderflow) {this.setCustomValidity(`' . $this->getSetting('min_strokes_message') . '`);}',
        ],
      ];
    }

    $element['signature_pad_canvas']['#attributes']['data-drupal-signature-pad-settings'] = json_encode($element['#signature_pad_settings']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $formState, $form) {
    $element = parent::process($element, $formState, $form);

    array_splice(
      $element['#pre_render'],
      1,
      0,
      [[RenderCallback::class, 'managedFile']]
    );

    return $element;
  }

  /**
   * Return an array of image formats supported.
   *
   * @return array<string,string>
   *   The map for the mime types to the extensions.
   */
  protected static function formatsMap() : array {
    return [
      'image/png' => 'png',
      'image/jpeg' => 'jpg',
      'image/svg+xml' => 'svg',
    ];
  }

  /**
   * Inspect metadata of a PNG to extract the json string.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $imageItem
   *   Field data.
   */
  protected function getInitDataPng($imageItem) : ?string {
    /** @var \Drupal\file\Entity\File $file */
    $file = $imageItem->entity;
    $metadata = PNGMetadata::extract($file->getFileUri());
    if (!$metadata) {
      return NULL;
    }
    $metadata = $metadata->toArray();
    if (empty($metadata['Comment']) || empty($metadata['Comment'][''])) {
      return NULL;
    }
    return $metadata['Comment'][''];
  }

  /**
   * Inspect metadata of a SVG to extract the json string.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $imageItem
   *   Field data.
   */
  protected function getInitDataSvg($imageItem) : ?string {
    /** @var \Drupal\file\Entity\File $file */
    $file = $imageItem->entity;
    $svg = new \DOMDocument();
    $svg->loadXML(file_get_contents($file->getFileUri()));
    $xpath = new \DOMXPath($svg);
    $xpath->registerNamespace('svg', 'http://www.w3.org/2000/svg');
    $sourceCode = $xpath->query('//*[name()="metadata"]//*[name()="schema:SoftwareSourceCode"]//*[name()="schema:text"]');
    if ($sourceCode->length !== 1) {
      return NULL;
    }
    return $sourceCode->item(0)->textContent;
  }

}
