<?php

declare(strict_types = 1);

namespace Drupal\signature_pad\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a Minimum strokes constraint.
 *
 * @Constraint(
 *   id = "SignaturePadMinStrokes",
 *   label = @Translation("Minimum strokes", context = "Validation"),
 * )
 *
 * @see https://www.drupal.org/node/2015723.
 */
final class MinimumStrokesConstraint extends Constraint {

  public int $minStrokes;

  public string $format;

  public string $message = 'Please add some strokes to the signature';

}
