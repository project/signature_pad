<?php

declare(strict_types = 1);

namespace Drupal\signature_pad\Plugin\Validation\Constraint;

use Drupal\file\FileInterface;
use PNGMetadata\PNGMetadata;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Minimum strokes constraint.
 */
final class MinimumStrokesConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $entity, Constraint $constraint): void {
    if (!$entity instanceof FileInterface) {
      throw new \InvalidArgumentException(
        sprintf('The validated value must be instance of \Drupal\file\FileInterface, %s was given.', get_debug_type($entity))
      );
    }
    if ($entity->getMimeType() !== $constraint->format) {
      $this->context->addViolation('The uploaded image does not match the MIME type.');
      return;
    }

    if ($entity->getMimeType() === 'image/png') {
      $metadata = PNGMetadata::extract($entity->getFileUri());
      if (!$metadata) {
        $this->context->addViolation($constraint->message);
        return;
      }
      $metadata = $metadata->toArray();
      if (
        empty($metadata['Comment']) ||
        empty($metadata['Comment']['']) ||
        count(json_decode($metadata['Comment'][''])) < $constraint->minStrokes
      ) {
        $this->context->addViolation($constraint->message);
        return;
      }
    }

    if ($entity->getMimeType() === 'image/svg+xml') {
      $svg = new \DOMDocument();
      $svg->strictErrorChecking = FALSE;
      $svg->loadXML(file_get_contents($entity->getFileUri()));
      $xpath = new \DOMXPath($svg);
      $xpath->registerNamespace('svg', 'http://www.w3.org/2000/svg');
      $sourceCode = $xpath->query('//*[name()="metadata"]//*[name()="schema:SoftwareSourceCode"]//*[name()="schema:text"]');
      if ($sourceCode->length !== 1) {
        $this->context->addViolation($constraint->message);
        return;
      }
      $data = json_decode($sourceCode->item(0)->textContent);
      if (count($data) < $constraint->minStrokes) {
        $this->context->addViolation($constraint->message);
      }
    }
  }

}
