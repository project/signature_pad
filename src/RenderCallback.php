<?php

namespace Drupal\signature_pad;

use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Implements trusted prerender callbacks for signature pad.
 */
class RenderCallback implements TrustedCallbackInterface {

  /**
   * Prerender callback for managed_file.
   *
   * @param array $element
   *   The form element for managed file.
   */
  public static function managedFile($element) : array {
    // Always display the file upload.
    if (array_key_exists('#access', $element['upload'])) {
      $element['upload']['#access'] = TRUE;
    }

    // Description about number of files, file formats
    // and sizes is not very useful in the context of the signature pad.
    $element['#description_display'] = 'invisible';
    $element['upload']['#attributes']['class'][] = 'visually-hidden';

    // Hide image links.
    if ($element['#signature_pad_settings']['hideImageLink']) {
      $fileKeys = array_filter(Element::children($element), function ($key) {
        return str_starts_with($key, 'file_');
      });
      foreach ($fileKeys as $fileKey) {
        $element[$fileKey]['#access'] = FALSE;
      }
    }

    if ($element['#signature_pad_settings']['hideRemoveButton']) {
      $element['remove_button']['#access'] = FALSE;
    }

    if ($element['#signature_pad_settings']['hideAltField']) {
      $element['alt']['#access'] = FALSE;
    }

    if ($element['#signature_pad_settings']['hideTitleField']) {
      $element['title']['#access'] = FALSE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'managedFile',
    ];
  }

}
