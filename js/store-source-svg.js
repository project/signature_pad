/**
 * @file
 * Signature pad save source.
 */

(function (Drupal, once) {

  'use strict';

  /**
   * Listen to images being saved.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.signaturePadStoreSourceSvg = {
    attach: function (context) {
      once('signature-pad-store-source', 'canvas[data-drupal-signature-pad-settings]', context).forEach(canvas => {
        canvas.addEventListener('signaturePadSyncImageCreated', (event) => {
          const settings = event.detail.settings;
          if (settings.format !== 'image/svg+xml') {
            return;
          }

          let svgString = canvas.signaturePad.toSVG();

          if (settings.saveJsonData) {
            const parser = new DOMParser();
            const svgXMLDocument = parser.parseFromString(svgString, 'image/svg+xml');
            const svgElement = svgXMLDocument.documentElement;
            const metadata = svgXMLDocument.createElementNS('http://www.w3.org/2000/svg', 'metadata');
            const rdf = svgXMLDocument.createElementNS('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'rdf:RDF');
            const sourceCode = svgXMLDocument.createElementNS('http://schema.org/','schema:SoftwareSourceCode');
            const sourceData = svgXMLDocument.createElementNS('http://schema.org/','text');
            sourceData.textContent = JSON.stringify(canvas.signaturePad.toData());
            sourceCode.appendChild(sourceData);
            rdf.appendChild(sourceCode);
            metadata.appendChild(rdf);
            svgElement.insertBefore(metadata, svgElement.firstChild);
            svgString = new XMLSerializer().serializeToString(svgElement);
          }

          const file = new File([svgString], `${settings.filename}.${settings.extension}`, {type: settings.format});
          const dataTransfer = new DataTransfer();
          dataTransfer.items.add(file);
          event.detail.fileInput.files = dataTransfer.files;
        });
      });
    }
  };

})(Drupal, once);
