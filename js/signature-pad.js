/**
 * @file
 * JavaScript behaviors for signature pad integration.
 */

(function (Drupal, once, debounce, SignaturePad) {

  'use strict';

  /**
   * Initialize signature element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.signaturePad = {
    attach: function (context) {
      once('signature-pad-behavior', 'canvas[data-drupal-signature-pad-settings]', context).forEach(canvas => {
        const wrapper = canvas.parentElement;
        const input = wrapper.querySelector('input[type="file"]');
        const form = wrapper.closest('form');
        const settings = JSON.parse(canvas.dataset.drupalSignaturePadSettings);

        const ratioWidth = parseInt(settings.aspectRatio.split(':')[0]);
        const ratioHeight = parseInt(settings.aspectRatio.split(':')[1]);

        // Drupal core file.js inside file module disables the file input during any clicking of a submit button.
        // To avoid that behavior the class that triggers that disabling is removed here.
        // It should be safe since the signature_pad widget doesn't trigger any ajax.
        input.classList.remove('js-form-file');
        // Hide the label that would trigger a file upload dialog.
        const label = document.querySelector(`label[for="${input.id}"]`);
        if (label) {
          label.style.display = 'none';
        }

        // Initialize signature canvas.
        const options = {
          backgroundColor: settings.backgroundColor,
          penColor: settings.penColor
        };
        canvas.signaturePad = new SignaturePad(canvas, options);

        const resetButton = wrapper.querySelector('.signature-pad-reset-button');
        if (resetButton) {
          resetButton.addEventListener('click', (event) => {
            event.preventDefault();
            canvas.signaturePad.clear();
            input.value = '';
            // Trigger the event even with no changes to clear the input.
            input.dispatchEvent(new Event('change'));
            return false;
          });
        }

        const undoButton = wrapper.querySelector('.signature-pad-undo-button');
        if (undoButton) {
          undoButton.addEventListener('click', (event) => {
            event.preventDefault();
            let data = canvas.signaturePad.toData();

            if (data) {
              // Remove the last dot or line.
              data.pop();
              canvas.signaturePad.fromData(data);
            }
            return false;
          });
        }

        const nativeColorPicker = wrapper.querySelector('.signature-pad-color-picker-native');
        if (nativeColorPicker) {
          nativeColorPicker.addEventListener('change', (event) => canvas.signaturePad.penColor = event.target.value, false);
        }

        const sizePicker = wrapper.querySelector('.signature-pad-size-picker-slider');
        if (sizePicker) {
          sizePicker.addEventListener('input', function () {
            canvas.signaturePad.maxWidth = parseInt(this.value);
            sizePicker.style.setProperty('--slider-dot-size', `${parseInt(this.value) + 10}px`);
          });
        }

        const strokes = wrapper.querySelector('.signature-pad-strokes');
        if (strokes) {
          canvas.signaturePad.addEventListener('endStroke', debounce(() => {
            let data = canvas.signaturePad.toData();
            strokes.value = data.length;
            // Trigger the change to remove the custom validity message.
            strokes.dispatchEvent(new Event('change'));
          }, 2000, false));

          // Handle the canvas being cleared.
          input.addEventListener('change', (event) => {
            strokes.value = 0;
          });
        }

        const refresh = () => {
          const data = canvas.signaturePad.toData();
          // Set dimensions.
          // Ignore the ratio when loading an image because increasing the dimensions
          // of the canvas will also increase the dimensions of the resulting image.
          const ratio = settings.dimensionsFromImage ? 1 : Math.max(window.devicePixelRatio || 1, 1);

          let width = settings.fixedWidth;
          if (!width) {
            width = wrapper.offsetWidth;
          }
          else {
            canvas.style.width = `${width}px`;
          }

          canvas.width = Math.round(ratio * width);

          if (settings.fixedHeight) {
            canvas.style.height = `${settings.fixedHeight}px`;
            canvas.height = Math.round(ratio * settings.fixedHeight);
          }
          else {
            canvas.height = Math.round(ratio * width * ratioHeight / ratioWidth);
          }

          canvas.getContext('2d').scale(ratio, ratio);
          canvas.signaturePad.clear();

          if (canvas.dataset.drupalSignaturePadInitUri) {
            // Load the data from the element.
            canvas.signaturePad.fromDataURL(canvas.dataset.drupalSignaturePadInitUri, {
              ratio: ratio,
              width: Math.round(canvas.dataset.drupalSignaturePadInitUriWidth / ratio),
              height: Math.round(canvas.dataset.drupalSignaturePadInitUriHeight / ratio)
            });
            if (settings.format === 'image/svg+xml') {
              console.log('The image was initialized with raster data that will not be saved to the SVG. Enable the option to save json in the SVG to have the ability of editing SVG.');
            }
          }

          if (canvas.dataset.drupalSignaturePadInitData) {
            // Load the data from the element.
            canvas.signaturePad.fromData(JSON.parse(canvas.dataset.drupalSignaturePadInitData));
            delete canvas.dataset.drupalSignaturePadInitData;
          }
          else {
            canvas.signaturePad.fromData(data);
          }
        };

        window.addEventListener('resize', debounce(refresh, 250));
        // Ensure the signature pad is initialized.
        setTimeout(refresh, 1);

        form.addEventListener('submit', (event) => {
          if (
            // Removing the image can "disconnect" the canvas from the DOM.
            !canvas.isConnected ||
            // No image has been painted or no new data in the image.
            !canvas.signaturePad.toData().length
          ) {
            return;
          }

          const syncImageEvent = new CustomEvent('signaturePadSyncImageCreated', {
            detail: {
              fileInput: input,
              settings: settings,
            },
            bubbles: false,
            cancelable: false,
            composed: false,
          });
          canvas.dispatchEvent(syncImageEvent);

          if (!form.querySelector('canvas[data-drupal-signature-pad-processing-scheduled]')) {
            return;
          }

          // For other cases convert the canvas to a blob.
          // Since toBlob() is asynchronous it won't finish before the actual submit
          // so it needs to stop and do the submit only when when the image has been
          // set in the file input.
          // @todo: Make it work for multiple fields.
          event.preventDefault();
          // The input element with name="op" needs to be submitted
          // and halting the submit prevents that.
          // Adding a hidden input
          // forces that value to be sent.
          const hiddenSubmitValue = document.createElement('input');
          hiddenSubmitValue.type = 'hidden';
          hiddenSubmitValue.name = event.submitter.name;
          hiddenSubmitValue.value = event.submitter.value;
          event.submitter.parentNode.appendChild(hiddenSubmitValue);

          canvas.toBlob((imageBlob) => {
            imageBlob.arrayBuffer().then((arrayBuffer) => {

              // Callback that takes the arrayBuffer and puts it in the file input.
              // Then checks if other processing is finished.
              const arrayBufferToFileInput = (arrayBuffer, settings, fileInput, form, canvas) => {
                const file = new File([arrayBuffer], `${settings.filename}.${settings.extension}`, {type: settings.format});
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(file);
                fileInput.files = dataTransfer.files;

                delete canvas.dataset.drupalSignaturePadProcessingScheduled;

                if (!form.querySelector('canvas[data-drupal-signature-pad-processing-scheduled]')) {
                  form.submit();
                }
              };

              if (settings.saveJsonData) {
                const imageEvent = new CustomEvent('signaturePadImageCreated', {
                  detail: {
                    arrayBuffer: arrayBuffer,
                    fileInput: input,
                    settings: settings,
                    form: form,
                    arrayBufferToFileInput: arrayBufferToFileInput
                  },
                  bubbles: false,
                  cancelable: false,
                  composed: false,
                });
                canvas.dispatchEvent(imageEvent);
                return;
              }

              // No processing for this image.
              arrayBufferToFileInput(arrayBuffer, settings, input, form, canvas);
            });
          }, settings.format);
        });
      });
    }
  };
})(Drupal, once, Drupal.debounce, SignaturePad);
