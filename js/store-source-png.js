/**
 * @file
 * Signature pad save source.
 */

(function (Drupal, once, MetaPNG) {

  'use strict';

  /**
   * Listen to images being saved.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.signaturePadStoreSourcePng = {
    attach: function (context) {
      once('signature-pad-store-source', 'canvas[data-drupal-signature-pad-settings]', context).forEach(canvas => {
        canvas.addEventListener('signaturePadImageCreated', (event) => {
          const array = new Uint8Array(event.detail.arrayBuffer);
          const newArrayBuffer = MetaPNG.addMetadata(
            array,
            'Comment',
            JSON.stringify(event.target.signaturePad.toData())
          );

          event.detail.arrayBufferToFileInput(
            newArrayBuffer,
            event.detail.settings,
            event.detail.fileInput,
            event.detail.form,
            event.target,
          );
        });
      });
    }
  };

})(Drupal, once, MetaPNG);
